<?php
    include "db.php";
    include "query.php";
?>
<html>
    <head>
        <title>ex 4</title>  
    </head>
    <body>
        <p>
        <?php
                    // database connection
                    $db = new DB('localhost','intro','root',''); 
                    $dbc = $db->connect();  
                    
                    $query = new query($dbc); 
                    //the books with the name of the user that the book belongs to
                    $q = "SELECT u.name, b.title
                        FROM users u
                        JOIN books b
                        ON u.id = b.user_fk";
                    $result = $query->query($q); 
                    echo '<br>';
                    //echo $result->num_rows; //for test
                    if($result->num_rows >0){ 
                        echo '<table>';       
                        echo '<tr><th>Name of the user</th><th>Name of the book</th></tr>';
                        while($row = $result->fetch_assoc()){      
                            echo '<tr>';
                            echo '<td>'.$row['name'].'</td><td>'.$row['title'].'</td>';
                            echo '</tr>';
                        }
                        echo '</table>';
                    }   else{
                        echo "Sorry no results";
                    }    
                
                ?>  
                </P>
            </body>
            
        </html>